package restaurant;

<<<<<<< HEAD
import java.util.LinkedList; 
import java.util.List; 
import java.util.Map;
import org.junit.Assert;
import com.google.common.collect.ImmutableMap;
import restaurant.Game;
import cucumber.api.java.Before;
=======
import java.util.ArrayList;
import java.util.List; 

import org.junit.Assert;
import org.mockito.Mockito;

import restaurant.Game;
import cucumber.api.DataTable;
>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
import cucumber.api.java.en.Given; 
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RestaurantSteps {
<<<<<<< HEAD

	Game game=null;
    private List<Map> menuList = new LinkedList<Map>();
    private List<Map> ingList = new LinkedList<Map>();
    private List<Map> clientList = new LinkedList<Map>();
    private List<Map> waiter = new LinkedList<Map>();
    private List<Map> barman = new LinkedList<Map>();
    private List<Map> chef = new LinkedList<Map>();
    
    @Before
   	public void setup() {
   		game = new Game();}
       
  	@Given("^The \"([^\"]*)\" is created with the name \"([^\"]*)\"$")
   	public void The_is_created_with_the_name(String arg1, String arg2) throws Throwable {
   		if(arg1 == "restaurant"){
   			game.setRtype(arg1);
   			game.setRname(arg2);}
   		else{
   			game.setPtype(arg1);
   			game.setPname(arg2);}
   	}
   	
   	@Given("^The restaurant budget is initialised to (\\d+)$")
   	public void The_restaurant_budget_is_initialised_to(int arg1) throws Throwable {
   		game.setBudget(arg1);
   	}
   	
   	@When("^I start playing restaurant game$")
   	public void I_start_playing_restaurant_game() throws Throwable {
   		Assert.class.getDeclaredConstructor().equals(game);
   	}
   	
   	@Then("^I should see \"([^\"]*)\"$")
   	public void I_should_see(String arg1) throws Throwable {
   			Assert.assertNotNull(arg1,game);
   	}
   	
   	@Given("^The game has started$")
   	public void The_game_has_started() throws Throwable {
   		Assert.assertNotNull(game);
   	}

   	@Given("^The restaurant is created$")
   	public void The_restaurant_is_created() throws Throwable {
   		Assert.assertTrue(game.getRtype()!="");
   	}

   	@Given("^the following set of \"([^\"]*)\"$")
   	public void the_following_set_of(String arg1, List<Map> maps) throws Throwable {
   			for(Map map : maps)
   			{
   	            menuList.add(map);}
   		}

   	@When("^I create a Menu$")
   	public void I_create_a_Menu() throws Throwable {
   		game.setMenu(menuList);

   	}
   	
   	@Then("^I should see the \"([^\"]*)\"$")
   	public void I_should_see_the(String arg1) throws Throwable {		
   		Assert.assertEquals(arg1, game.check());
   	}  
   	
	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int arg1) throws Throwable {
		game.setBudget(arg1);
=======
	
	Game game = null;
	Game inggame= null;
	Game menu = null;
	Game ingmenu = null;
    private List<Game> dishList = new ArrayList<Game>();
    private List<Game> beverageList = new ArrayList<Game>();
    private List<Game> ingList = new ArrayList<Game>();
    private List<Game> chooseList = new ArrayList<Game>();
    private List<Game>	waiterList = new ArrayList<Game>();
    private List<Game>	chefList = new ArrayList<Game>();
    private List<Game>	barmanList = new ArrayList<Game>();

	@Given("^The \"([^\"]*)\" is created with the name \"([^\"]*)\"$")
	public void The_is_created_with_the_name(String arg1, String arg2) throws Throwable {
		game = new Game();
		if(arg1 == "restaurant")
		{
			game.setRtype(arg1);
			game.setRname(arg2);
		}
		
		else
		{
			game.setPtype(arg1);
			game.setPname(arg2);
		}
	}
	
	@Given("^The restaurant budget is initialised to (\\d+)$")
	public void The_restaurant_budget_is_initialised_to(int arg1) throws Throwable {
		game.setBudget(arg1);
	}
	
	@When("^I start playing restaurant game$")
	public void I_start_playing_restaurant_game() throws Throwable {
		Assert.class.getDeclaredConstructor().equals(game);
	}
	
	@Then("^I should see \"([^\"]*)\"$")
	public void I_should_see(String arg1) throws Throwable {
		Assert.assertNotNull(arg1,game);
		
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
		game= new Game();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
		game.getRtype();
	}

	@Given("^the following set of \"([^\"]*)\"$")
	public void the_following_set_of(String arg1, DataTable dataTable) throws Throwable {
		if(arg1 == "Dishes")
				dishList=dataTable.asList(Game.class);
		else
	            beverageList=dataTable.asList(Game.class);			
		}

	@When("^I create a Menu$")
	public void I_create_a_Menu() throws Throwable {
	    menu = new Game(dishList,beverageList);
	}

	@Then("^I should see the \"([^\"]*)\"$")
	public void I_should_see_the(String arg1) throws Throwable {		
		if(arg1 == "Restaurant menu is created" && dhighcheck() && dlowcheck() && bhighcheck() && blowcheck()== true)
			Assert.assertNotNull(arg1, menu);
		else if(arg1 == "ERROR!!! All the high quality dishes must have the same price" && dhighcheck()== false && dlowcheck() && bhighcheck() && blowcheck()== true)
			Assert.assertNotNull(arg1, menu);
		else if(arg1 == "ERROR!!! All the low quality dishes must have the same price" && dlowcheck() == false && dhighcheck() && bhighcheck() && blowcheck()== true)
			Assert.assertNotNull(arg1, menu);
		else if(arg1 == "ERROR!!! All the high quality beverages must have the same price" && bhighcheck()== false && dhighcheck() && dlowcheck() && blowcheck()== true)
			Assert.assertNotNull(arg1, menu);
		else if(arg1 == "ERROR!!! All the low quality dishes must have the same price" && blowcheck()== false && dhighcheck() && dlowcheck() && bhighcheck()== true)
			Assert.assertNotNull(arg1, menu);
			}

	int i,j=0,k=0;
    int dcount=dishList.size();
    int bcount=beverageList.size();
	int a[]=new int[dcount];
	
	private boolean dhighcheck(){
		for(i=0; i<dcount;i++)
		{ if(dishList.get(i).getQuality()== "high")
			{a[j]=dishList.get(i).getCost();
				j++;}}
		for(i=0+1;i<=j;i++)
		{ if(a[0]==a[i])
			{ a[0]=a[i];
			k=k+1;}	}
		if(k==j)
			{i=0;j=0;k=0;
			return true;}
		else
			{i=0;j=0;k=0;
			return false;}
	}

	private boolean dlowcheck(){
		for(i=0; i<dcount;i++)
		{ if(dishList.get(i).getQuality()== "low")
			{a[j]=dishList.get(i).getCost();
				j++;}}
		for(i=0+1;i<=j;i++)
		{ if(a[0]==a[i])
			{ a[0]=a[i];
			k=k+1;}	}
		if(k==j)
			{i=0;j=0;k=0;
			return true;}
		else
			{i=0;j=0;k=0;
			return false;}
	}
	
	private boolean bhighcheck(){
		for(i=0; i<bcount;i++)
		{ if(beverageList.get(i).getQuality()== "high")
			{a[0]=beverageList.get(i).getCost();
				j++;}}
		for(i=0+1;i<=j;i++)
		{ if(a[0]==a[i])
			{ a[0]=a[i];
			k=k+1;}	}
		if(k==j)
			{i=0;j=0;k=0;
			return true;}
		else
			{i=0;j=0;k=0;
			return false;}
	}
	
	private boolean blowcheck(){
		for(i=0; i<bcount;i++)
		{ if(beverageList.get(i).getQuality()== "low")
			{a[0]=beverageList.get(i).getCost();
				j++;}}
		for(i=0+1;i<=j;i++)
		{ if(a[0]==a[i])
			{ a[0]=a[i];
			k=k+1;}	}
		if(k==j)
			{i=0;j=0;k=0;
			return true;}
			else
			{i=0;j=0;k=0;
			return false;}
	}

	@Given("^The budget is (\\d+)$")
	public void The_budget_is(int arg1) throws Throwable {
		ingmenu= new Game();
		ingmenu.setBudget(arg1);
>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
	}

	@Given("^cost of ingredients for \"([^\"]*)\" is (\\d+)$")
	public void cost_of_ingredients_for_is(String arg1, int arg2) throws Throwable {
<<<<<<< HEAD
		ingList.add(ImmutableMap.of("item",arg1,"cost",arg2));
				
=======
		ingmenu.setItemtype(arg1);
		ingmenu.setCost(arg2);
		ingList.add(ingmenu);
>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
	}

	@Given("^(\\d+) clients choose \"([^\"]*)\"$")
	public void clients_choose(int arg1, String arg2) throws Throwable {
<<<<<<< HEAD
		clientList.add(ImmutableMap.of("item",arg2,"cnumber",arg1));
		
=======
	    ingmenu.setCchoose(arg1);
	    ingmenu.setClientitem(arg2);
	    chooseList.add(ingmenu);
>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
	}

	@When("^The budget is updated based on the \"([^\"]*)\"$")
	public void The_budget_is_updated_based_on_the(String arg1) throws Throwable {
<<<<<<< HEAD
		game.setUpdatebase(arg1);
		game.setIng(ingList);
		game.setClient(clientList);
		game.setWaiterList(waiter);
		game.setBarmanList(barman);
		game.setChefList(chef);
	}
	
	@Then("^The budget should be (\\d+)$")
	public void The_budget_should_be(int arg1) throws Throwable {
			game.budgetCal();
			Assert.assertEquals(arg1, game.getBudget());
	}

	@Then("^Game Over$")
	public void Game_Over() throws Throwable {
	    if(game.getBudget() == 0)
=======
		ingmenu.setUpdatebase(arg1);
	}

	@Then("^The budget should be (\\d+)$")
	public void The_budget_should_be(int arg1) throws Throwable {
		budgetCal(arg1);
	}

	private void budgetCal(int arg1)
	{	k=ingmenu.getBudget();
	if(ingmenu.getUpdatebase() == "expenses")
		{for(i=0;i<ingList.size();i++)
			{for(j=0;j<ingList.size();j++)
				{	if(ingList.get(i).getItemtype() == chooseList.get(j).getClientitem())
				{
					k = k -(ingList.get(i).getCost() * chooseList.get(j).getCchoose());
				}}}}
	if(ingmenu.getUpdatebase() == "salary")
		{for(i=0;i<waiterList.size();i++)
			k = k - waiterList.get(i).getSalary();
		 for(i=0;i<chefList.size();i++)
			k = k - chefList.get(i).getSalary();
		 for(i=0;i<barmanList.size();i++)
			k = k - barmanList.get(i).getSalary();			
		}
	if(k==arg1)
		Assert.assertEquals(arg1, ingmenu.getUpdatebase());
	}
	@Then("^Game Over$")
	public void Game_Over() throws Throwable {
	    if(ingmenu.getBudget()==0)
>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
	    {	game = null;}
	    	
	}

	@Given("^The restaurant has the following employee\\(s\\) as \"([^\"]*)\"$")
<<<<<<< HEAD
	public void The_restaurant_has_the_following_employee_s_as(String arg1, List<Map> map) throws Throwable {
		if(arg1.matches("waiters"))
			for(Map waiters : map)
			{waiter.add(waiters);}
		else if(arg1.matches("chef"))
			for(Map chefs : map)
			{chef.add(chefs);}
		else
			for(Map barmans : map)
			{barman.add(barmans);}				            
	}

=======
	public void The_restaurant_has_the_following_employee_s_as(String arg1, DataTable arg2) throws Throwable {
				if(arg1 == "waiters")
					waiterList=arg2.asList(Game.class);
				else if (arg1 == "chef")
					chefList=arg2.asList(Game.class);
				else
					barmanList=arg2.asList(Game.class);
	}


>>>>>>> 729e0c0d77f0a6d9bb23751874505d1681b23367
}
